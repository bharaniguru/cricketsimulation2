
# Cricket Score Simulation - PHP Programme to generate score and commentatory with Over and Runs to win

## Features
- Programme to simulate match reslut for target run and overs remain
- Using random generated number, we are determining the runs scorred per ball
- We have list for players and thier probability of scoring runs
- Depend on players probability, we are simulating result
- Batsman change strick at every end of every over
- We haven't considered no balls, wide balls and other runs, therefore an over is always 6 balls

## How to run application
- Navigate to apache folder, and place or clone our complete application folder
- Go to browser, run link like (http://localhost/CricketSimulation)
- Now you can see expected result
- if you want to change overs left and target score, please change in index.php file

```php
<?php
include 'MatchConfig.php';
include 'Cricket.php';

MatchConfig::$playersProbability = array(
	array(
		'PLAYER_NAME' => 'Kirat Boli',
		'PLAYER_PROBABILITY' => array('0' => 5,'1' => 30,'2' => 25,'3' => 10,'4' => 15,'5' => 1,'6' => 9,'OUT' => 5)
	),
	array(
		'PLAYER_NAME' => 'N.S Nodhi',
		'PLAYER_PROBABILITY' => array('0' => 10,'1' => 40,'2' => 20,'3' => 5,'4' => 10,'5' => 1,'6' => 10,'OUT' => 4)
	),
	array(
		'PLAYER_NAME' => 'R Rumrah',
		'PLAYER_PROBABILITY' => array('0' => 20,'1' => 30,'2' => 15,'3' => 5,'4' => 5,'5' => 1,'6' => 4,'OUT' => 20)
	),
	array(
		'PLAYER_NAME' => 'Shashi Hendra',
		'PLAYER_PROBABILITY' =>  array('0' => 30,'1' => 25,'2' => 5,'3' => 0,'4' => 5,'5' => 1,'6' => 4,'OUT' => 30)
	)
);

MatchConfig::$oversLeft = 4;
MatchConfig::$runsToWin = 40;
MatchConfig::$wicketsLeft = 3;
``` 

## Installation of phpunit test
To install phpunit, we need composer need to be installed(https://getcomposer.org/). and add following to composer.json 

```json
"phpunit/phpunit": "^8.3"
```

and run 

```sh
composer install
```

or run

```sh
composer require --dev phpunit/phpunit ^8.3
```

## How to run phpunit test for this application

Navigate to application directory(e.g., cd ./CricketSimulation) and run below command

```sh
./vendor/phpunit/phpunit/phpunit unittest/_file_name_
```


Now you can see results for unitest result
