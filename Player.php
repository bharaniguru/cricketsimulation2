<?php

/**
 *  Player class used to do individual Player functionals
 */
class Player
{
	public $stricker;
	public $nonStricker;

	function __construct($stricker, $nonStricker)
	{
		$this->stricker = $stricker;
		$this->stricker['runs'] = 0;
		$this->stricker['balls'] = 0;
		$this->stricker['wicket'] = FALSE;

		$this->nonStricker = $nonStricker;
		$this->nonStricker['runs'] = 0;
		$this->nonStricker['balls'] = 0;
		$this->nonStricker['wicket'] = FALSE;
	}

	function ChangeStrick(){
		$tmp = $this->nonStricker;
		$this->nonStricker = $this->stricker;
		$this->stricker = $tmp;
	}

	function SetNewBatsman($newBatsman)
	{
		$this->stricker = $newBatsman;
		$this->stricker['runs'] = 0;
		$this->stricker['balls'] = 0;
		$this->stricker['wicket'] = FALSE;
	}

	function SetIndividualScore($runs, $wickets = FALSE){
		$this->stricker['runs'] += $runs;
		$this->stricker['balls'] += 1;
		$this->stricker['wicket'] = $wickets;
	}
	
}

?>