<?php
include 'MatchConfig.php';
include 'Cricket.php';


/**
 *
 * You can change below MatchConfig values, accordingly It will simulate match result
 *
 */

MatchConfig::$playersProbability = array(
	array(
		'PLAYER_NAME' => 'Kirat Boli',
		'PLAYER_PROBABILITY' => array('0' => 5,'1' => 30,'2' => 25,'3' => 10,'4' => 15,'5' => 1,'6' => 9,'OUT' => 5)
	),
	array(
		'PLAYER_NAME' => 'N.S Nodhi',
		'PLAYER_PROBABILITY' => array('0' => 10,'1' => 40,'2' => 20,'3' => 5,'4' => 10,'5' => 1,'6' => 10,'OUT' => 4)
	),
	array(
		'PLAYER_NAME' => 'R Rumrah',
		'PLAYER_PROBABILITY' => array('0' => 20,'1' => 30,'2' => 15,'3' => 5,'4' => 5,'5' => 1,'6' => 4,'OUT' => 20)
	),
	array(
		'PLAYER_NAME' => 'Shashi Hendra',
		'PLAYER_PROBABILITY' =>  array('0' => 30,'1' => 25,'2' => 5,'3' => 0,'4' => 5,'5' => 1,'6' => 4,'OUT' => 30)
	)
);

MatchConfig::$oversLeft = 4;
MatchConfig::$runsToWin = 40;
MatchConfig::$wicketsLeft = 3;

$cricket = new Cricket;
$simulation = $cricket->play();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Cricket Simulation</title>
</head>
<body>
	<h3><?=$simulation->marchResult?></h3>
	<ul style="list-style: none;">
		<?php foreach ($simulation->playerScoreboard as $key => $value): ?>
			<li>
				<?=$value['PLAYER_NAME']?> <?=$value['wicket'] ? '' : '*'?>  - <?=$value['runs']?> (<?=$value['balls']?> balls)
			</li>	
		<?php endforeach ?>
	</ul>
	
	<h4>Commentary:</h4>
	<ul style="list-style: none;">
		<?php foreach ($simulation->commentary as $key => $value): ?>
			<li>
				<?=$value?>
			</li>	
		<?php endforeach ?>
	</ul>
</body>
</html>