<?php
include 'Match.php';
include 'MatchConfig.php';

use PHPUnit\Framework\TestCase;

class TestMatch extends TestCase
{
	
	public function testPlayersScoreBoard()
	{

		$testA = 'Test';
		$match = new Match;
		
		$match->SetPlayerScoreboard($testA);

		$this->assertTrue(in_array($testA, $match->playerScoreboard));
	}

	public function testCommentary()
	{
		$testB = 'Test';
		$match = new Match;
		
		$match->SetCommentary($testB);
		$this->assertTrue(in_array($testB, $match->commentary));
	}

	public function testGetStrickResult()
    {
        $playerA = array('0' => 5,'1' => 30,'2' => 25,'3' => 10,'4' => 15,'5' => 1,'6' => 9,'OUT' => 5);
        $match = new Match;

        $this->assertTrue(in_array($match->getStrickResult($playerA), [0,1,2,3,4,5,6,'OUT']));
    }
}
?>