<?php
include 'Player.php';
use PHPUnit\Framework\TestCase;

class TestPlayer extends TestCase
{
	public function testStrickerNonStricker()
	{
		$playerA = array('0' => 1,'1' => 30,'2' => 3,'3' => 10,'4' => 15,'5' => 1,'6' => 9,'OUT' => 5, 'runs' => 0, 'balls' => 0, 'wicket' => false);
        $playerB = array('0' => 5,'1' => 30,'2' => 25,'3' => 10,'4' => 15,'5' => 1,'6' => 9,'OUT' => 5, 'runs' => 0, 'balls' => 0, 'wicket' => false);

        $player = new Player($playerA, $playerB);

       	$this->assertEquals($player->stricker, $playerA);
        $this->assertEquals($player->nonStricker, $playerB);
	}


	public function testChangeStrick()
	{
		$playerA = array('0' => 1,'1' => 30,'2' => 3,'3' => 10,'4' => 15,'5' => 1,'6' => 9,'OUT' => 5, 'runs' => 0, 'balls' => 0, 'wicket' => false);
        $playerB = array('0' => 5,'1' => 30,'2' => 25,'3' => 10,'4' => 15,'5' => 1,'6' => 9,'OUT' => 5, 'runs' => 0, 'balls' => 0, 'wicket' => false);

        $player = new Player($playerA, $playerB);
        $player->ChangeStrick();

        $this->assertEquals($player->stricker, $playerB);
        $this->assertEquals($player->nonStricker, $playerA);
	}

	public function testSetIndividualScore()
	{
		$playerA = array('0' => 1,'1' => 30,'2' => 3,'3' => 10,'4' => 15,'5' => 1,'6' => 9,'OUT' => 5, 'runs' => 0, 'balls' => 0, 'wicket' => false);
        $playerB = array('0' => 5,'1' => 30,'2' => 25,'3' => 10,'4' => 15,'5' => 1,'6' => 9,'OUT' => 5, 'runs' => 0, 'balls' => 0, 'wicket' => false);

        $runA = 6;
        $runB = 6;

        $player = new Player($playerA, $playerB);
        
        $player->SetIndividualScore($runA);
        $player->SetIndividualScore($runB);

        $this->assertEquals($player->stricker['runs'], $runA + $runB);
	}

}