<?php
/**
 * MatchConfig used to set required parameters
 */
class MatchConfig
{
	public static $playersProbability;
	public static $oversLeft;
	public static $runsToWin;
	public static $wicketsLeft;
}

?>