<?php
include 'Player.php';
/**
 *
 * Match Class contains Match functions to perform cricket
 *
 */
class Match 
{
	public $commentary = array();
	public $playerScoreboard = array();
	public $matchResult;
	
	function __construct()
	{
		$this->playersProbabilityValue = MatchConfig::$playersProbability;
		$this->oversLeft = MatchConfig::$oversLeft;
		$this->runsToWin = MatchConfig::$runsToWin;
		$this->wicketsLeft = MatchConfig::$wicketsLeft;
		$this->ballsLeft = $this->oversLeft * 6;
	}

	function SetPlayerScoreboard($playerInfo){
		array_push($this->playerScoreboard, $playerInfo);
	}
	
	function SetCommentary($commentary){
		array_push($this->commentary, $commentary);
	}

	function getStrickResult($stickPossibilities)
	{
		if(is_array($stickPossibilities)){
			$randomPercentage = rand(1,100);
			$count = 0;
			foreach ($stickPossibilities as $key => $value) {
				$count += $value;
				if ($count >= $randomPercentage) break;
			}
			return $key;	
		}
		return false;
	}

	public function StartPlay()
	{
		$players = new Player(array_shift($this->playersProbabilityValue), array_shift($this->playersProbabilityValue));
		
		$this->SetCommentary("<br><b>".$this->oversLeft." overs left. ".$this->runsToWin." runs to win</b>");

		for ($i=1; $i <= $this->oversLeft * 6; $i++) { 
			$this->ballsLeft -= 1;

			$strickValue = $this->getStrickResult($players->stricker['PLAYER_PROBABILITY']);
			
			if($strickValue === 'OUT'){
				$this->SetCommentary($players->stricker['PLAYER_NAME'] . " Got OUT");
				
				$players->SetIndividualScore(0, TRUE);

				$this->wicketsLeft -= 1;

				if($this->wicketsLeft == 0){
			 		break;
				}

				$this->SetPlayerScoreboard($players->stricker);
				$players->SetNewBatsman(array_shift($this->playersProbabilityValue));

			}else{
				$this->SetCommentary($players->stricker['PLAYER_NAME'] ." scrores " . $strickValue . (($strickValue == 0 || $strickValue == 1 ) ? " Run " : " Runs "));

				$this->runsToWin -= $strickValue;
				$players->SetIndividualScore($strickValue);

				if($strickValue % 2 != 0){
					$players->ChangeStrick();
				}
			}

			if($this->runsToWin < 1){
				break;
			}

			if($i % 6 == 0 && $this->ballsLeft !=0 ){
				$players->changeStrick();
				$this->SetCommentary("<br><b>".($this->oversLeft - $i/6)." overs left. ".$this->runsToWin." runs to win</b>");
			}
		}

		$this->SetPlayerScoreboard($players->stricker);
		$this->SetPlayerScoreboard($players->nonStricker);
		$this->GetMatchResult();

		return $this;
	}

	public function GetMatchResult()
	{
		if($this->runsToWin == 1){
			$this->marchResult = "Bengaluru draw the match with ".$this->wicketsLeft." wicket remaining";
		}elseif($this->runsToWin > 1) {
			$this->marchResult = "Bengaluru lost by ".$this->runsToWin." runs and ".$this->ballsLeft." balls remaining";
		}else{
			$this->marchResult = "Bengaluru won by ".$this->wicketsLeft." wicket and ".$this->ballsLeft." balls remaining";
		}
	}
}

?>