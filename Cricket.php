<?php
include 'Match.php';
/**
 *
 * Cricket Class to Bootstrap Match 
 *
 */
class Cricket
{
	private $match;

	function __construct()
	{
		$this->match = new Match();
	}

	public function Play()
	{
		return $this->match->StartPlay();
	}
}
?>